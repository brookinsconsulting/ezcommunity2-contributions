<?
/*!
    $Id: ezquery.php,v 1.4 2000/08/14 09:18:57 bf-cvs Exp $

    Author: B�rd Farstad <bf@ez.no>
    
    Created on: 
    
    Copyright (C) 2000 eZ systems. All rights reserved.
*/

/*!
  En klasse som h�ndterer SQL queries. Lager query setninger fra
  tekststrenger. 
  
*/

class eZQuery
{
    function eZQuery( $fields, $queryText )
    {
        $this->Fields = $fields;
        $this->QueryText = $queryText;        
    }

    function buildQuery( )
    {
        $field = "KeyWords";

        $QueryText = $this->QueryText;
        
        $QueryText = trim( $QueryText );
        $QueryText = ereg_replace( "[ ]+", " ", $QueryText );
        $queryArray = explode( " ", $QueryText );

        $query = "";
        for ( $i=0; $i<count($queryArray); $i++ )            
        {
            for ( $j=0; $j<count($this->Fields); $j++ )
            {
                $queryItem = $queryArray[$i];
                if ( $queryItem[0] == "-" )
                {
                    $queryItem = ereg_replace( "^-", "", $queryItem );
                    $not = "NOT";
                }
                else
                    $not = "";
                
                $queryItem = $this->Fields[$j] ." " . $not . " LIKE '%" . $queryItem . "%' ";

                if ( $j > 0 )                    
                    $queryItem = "OR " . $queryItem . " ";
                    
            
                $query .= $queryItem;
            }

            if (  count( $queryArray) != ($i+1) )
                $query = " (" . $query . ") AND ";
            else
                $query = " (" . $query . ") ";
            
        }
        return $query;
    }

    var $Fields;
    var $QueryText;    
}
?>
