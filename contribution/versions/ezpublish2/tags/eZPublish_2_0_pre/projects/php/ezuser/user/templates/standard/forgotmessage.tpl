<!-- BEGIN user_not_exists_tpl -->
<p class="boxtext">{intl-user_not_exists}:</p>
{intl-user_not_exists_text}
<!-- END user_not_exists_tpl -->

<!-- BEGIN mail_sent_tpl -->
<p class="boxtext">{intl-mail_sent}:</p>
{intl-mail_sent_text}
<!-- END mail_sent_tpl -->


<!-- BEGIN generated_password_tpl -->
<p class="boxtext">{intl-generated_passord}:</p>
{intl-generated_passord_text}
<!-- END generated_password_tpl -->
