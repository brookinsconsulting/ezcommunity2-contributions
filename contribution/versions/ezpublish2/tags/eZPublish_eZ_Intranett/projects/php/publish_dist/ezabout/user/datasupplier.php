<?php
//
// $Id: datasupplier.php,v 1.11 2001/07/19 11:52:22 jakobn Exp $
//
// Created on: <23-Oct-2000 17:53:46 bf>
//
// This source file is part of eZ publish, publishing software.
//
// Copyright (C) 1999-2001 eZ Systems.  All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, US
//

include_once( "classes/ezpublish.php" );

$version = eZPublish::version();

?>
<h1>About eZ publish v <? echo $version; ?></h1>
<hr noshade="noshade" size="4" />
<br />

<p>
eZ publish is an Open Source portal building software, content manager or publishing solution whichever fits your needs. 
</p><p>
It is released under the <a href="http://www.gnu.org/copyleft/gpl.html
">GPL license</a> and can be downloaded from <a href="http://publish.ez.no">publish.ez.no</a>. You can get commercial support from eZ systems at
<a href="http://ez.no">ez.no</a> or at <a href="http://sourceprovider.com">sourceprovider.com</a>.<br />
</p>
