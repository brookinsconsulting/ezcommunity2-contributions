<?php
//
// $Id: productview.php,v 1.77.2.2.4.16 2002/04/22 08:29:45 ce Exp $
//
// Created on: <24-Sep-2000 12:20:32 bf>
//
// This source file is part of eZ publish, publishing software.
//
// Copyright (C) 1999-2001 eZ Systems.  All rights reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, US
//

include_once( "classes/INIFile.php" );
include_once( "classes/eztemplate.php" );
include_once( "classes/ezlocale.php" );
include_once( "classes/ezcurrency.php" );
include_once( "classes/eztexttool.php" );

include_once( "eztrade/classes/ezproduct.php" );
include_once( "eztrade/classes/ezproductcategory.php" );
include_once( "eztrade/classes/ezoption.php" );
include_once( "eztrade/classes/ezpricegroup.php" );
include_once( "eztrade/classes/ezproductcurrency.php" );
include_once( "eztrade/classes/ezproductpermission.php" );
include_once( "eztrade/classes/ezproductpricerange.php" );
include_once( "ezuser/classes/ezuser.php" );

include_once( "classes/ezmodulelink.php" );
include_once( "classes/ezlinksection.php" );
include_once( "classes/ezlinkitem.php" );

$ini =& INIFile::globalINI();

$Language = $ini->read_var( "eZTradeMain", "Language" );
$ShowPriceGroups = $ini->read_var( "eZTradeMain", "PriceGroupsEnabled" ) == "true";
$RequireUserLogin = $ini->read_var( "eZTradeMain", "RequireUserLogin" ) == "true";
$SimpleOptionHeaders = $ini->read_var( "eZTradeMain", "SimpleOptionHeaders" ) == "true";
$ShowQuantity = $ini->read_var( "eZTradeMain", "ShowQuantity" ) == "true";
$ShowNamedQuantity = $ini->read_var( "eZTradeMain", "ShowNamedQuantity" ) == "true";
$RequireQuantity = $ini->read_var( "eZTradeMain", "RequireQuantity" ) == "true" ;
$ShowOptionQuantity = $ini->read_var( "eZTradeMain", "ShowOptionQuantity" ) == "true";
$PurchaseProduct = $ini->read_var( "eZTradeMain", "PurchaseProduct" ) == "true";
$PricesIncludeVAT = $ini->read_var( "eZTradeMain", "PricesIncludeVAT" ) == "enabled" ? true : false;
$locale = new eZLocale( $Language );


$product = new eZProduct( $ProductID );

if ( $CategoryID == "" )
{
    $category = $product->categoryDefinition();
}
else
{
    $category = new eZProductCategory();
    $category->get( $CategoryID );
}

$CapitalizeHeadlines = $ini->read_var( "eZArticleMain", "CapitalizeHeadlines" );

$MainImageWidth = $ini->read_var( "eZTradeMain", "MainImageWidth" );
$MainImageHeight = $ini->read_var( "eZTradeMain", "MainImageHeight" );

$SmallImageWidth = $ini->read_var( "eZTradeMain", "SmallImageWidth" );
$SmallImageHeight = $ini->read_var( "eZTradeMain", "SmallImageHeight" );

$eZDebug = false;
if ( $GLOBALS["REMOTE_ADDR"] == "217.65.231.18" )
{
    $eZDebug = true;
}

// sections
include_once( "ezsitemanager/classes/ezsection.php" );

$GlobalSectionID = eZProductCategory::sectionIDStatic( $category->id() );

// init the section
$sectionObject =& eZSection::globalSectionObject( $GlobalSectionID );
$sectionObject->setOverrideVariables();

$user =& eZUser::currentUser();

if ( !isSet( $IntlDir ) )
    $IntlDir = "eztrade/user/intl";
if ( !isSet( $IniFile ) )
    $IniFile = "productview.php";



$TemplateDir =  $ini->read_var( "eZTradeMain", "TemplateDir" );

$t = new eZTemplate( "eztrade/user/" . $ini->read_var( "eZTradeMain", "TemplateDir" ),
                     $IntlDir, $Language, $IniFile );

$t->setAllStrings();

$sectionOverride = "_sectionoverride_$GlobalSectionID";

$t->set_var( "extra_product_info", "" );
if ( !isSet( $productview ) )
    $productview = "productview.tpl";

//if ( isSet( $template_array ) and isSet( $variable_array ) and
//     is_array( $template_array ) and is_array( $variable_array ) )
if ( eZFile::file_exists( "eztrade/user/$TemplateDir/productview" . $sectionOverride  . ".tpl" ) )
{
    $t->set_file( "product_view_tpl", "productview" . $sectionOverride  . ".tpl"  );
}
else
{
    $t->set_file( "product_view_tpl", "productview.tpl" );
}

$t->set_block( "product_view_tpl", "product_number_item_tpl", "product_number_item" );
$t->set_block( "product_view_tpl", "price_tpl", "price" );

$t->set_block( "product_view_tpl", "related_products_tpl", "related_products" );

$t->set_block( "product_view_tpl", "price_to_high_tpl", "price_to_high" );
$t->set_block( "product_view_tpl", "price_to_low_tpl", "price_to_low" );

$t->set_block( "product_view_tpl", "price_range_tpl", "price_range" );
$t->set_block( "product_view_tpl", "mail_method_tpl", "mail_method" );
$t->set_block( "price_range_tpl", "price_range_min_unlimited_tpl", "price_range_min_unlimited" );
$t->set_block( "price_range_tpl", "price_range_min_limited_tpl", "price_range_min_limited" );
$t->set_block( "price_range_tpl", "price_range_max_unlimited_tpl", "price_range_max_unlimited" );
$t->set_block( "price_range_tpl", "price_range_max_limited_tpl", "price_range_max_limited" );

$t->set_block( "price_tpl", "alternative_currency_list_tpl", "alternative_currency_list" );
$t->set_block( "alternative_currency_list_tpl", "alternative_currency_tpl", "alternative_currency" );

$t->set_block( "product_view_tpl", "quantity_item_tpl", "quantity_item" );
$t->set_block( "product_view_tpl", "add_to_cart_tpl", "add_to_cart" );
$t->set_block( "product_view_tpl", "voucher_buttons_tpl", "voucher_buttons" );
$t->set_block( "product_view_tpl", "first_tpl", "first" );
$t->set_block( "product_view_tpl", "path_tpl", "path" );
$t->set_block( "product_view_tpl", "image_list_tpl", "image_list" );

$t->set_block( "image_list_tpl", "image_tpl", "image" );
$t->set_block( "product_view_tpl", "main_image_tpl", "main_image" );
$t->set_block( "product_view_tpl", "option_tpl", "option" );
$t->set_block( "option_tpl", "value_price_header_tpl", "value_price_header" );
$t->set_block( "option_tpl", "value_tpl", "value" );

$t->set_block( "value_price_header_tpl", "value_description_header_tpl", "value_description_header" );
$t->set_block( "value_price_header_tpl", "value_price_header_item_tpl", "value_price_header_item" );
$t->set_block( "value_price_header_tpl", "value_currency_header_item_tpl", "value_currency_header_item" );

$t->set_block( "value_tpl", "value_description_tpl", "value_description" );
$t->set_block( "value_tpl", "value_price_item_tpl", "value_price_item" );
$t->set_block( "value_tpl", "value_availability_item_tpl", "value_availability_item" );
$t->set_block( "value_tpl", "value_price_currency_list_tpl", "value_price_currency_list" );

$t->set_block( "value_price_currency_list_tpl", "value_price_currency_item_tpl", "value_price_currency_item" );
$t->set_block( "product_view_tpl", "external_link_tpl", "external_link" );
$t->set_block( "product_view_tpl", "attribute_list_tpl", "attribute_list" );

$t->set_block( "attribute_list_tpl", "attribute_tpl", "attribute" );
$t->set_block( "attribute_list_tpl", "attribute_value_tpl", "attribute_value" );
$t->set_block( "attribute_list_tpl", "attribute_header_tpl", "attribute_header" );
$t->set_block( "attribute_value_tpl", "attribute_url_item_tpl", "attribute_url_item" );
$t->set_block( "attribute_value_tpl", "attribute_non_url_item_tpl", "attribute_non_url_item" );

$t->set_block( "product_view_tpl", "numbered_page_link_tpl", "numbered_page_link" );
$t->set_block( "product_view_tpl", "print_page_link_tpl", "print_page_link" );
$t->set_block( "product_view_tpl", "section_item_tpl", "section_item" );

$t->set_block( "add_to_cart_tpl", "kjop_item_tpl", "kjop_item" );
$t->set_block( "add_to_cart_tpl", "bestill_item_tpl", "bestill_item" );


$t->set_block( "section_item_tpl", "link_item_tpl", "link_item" );


if ( !isSet( $ModuleName ) )
    $ModuleName = "trade";
if ( !isSet( $ModuleList ) )
    $ModuleList = "productlist";
if ( !isSet( $ModuleView ) )
    $ModuleView = "productview";
if ( !isSet( $ModulePrint ) )
    $ModulePrint = "productprint";

$t->set_var( "module", $ModuleName );
$t->set_var( "module_list", $ModuleList );
$t->set_var( "module_view", $ModuleView );
$t->set_var( "module_print", $ModulePrint );
$t->set_var( "attribute_header", "" );
$t->set_var( "attribute_value", "" );
$t->set_var( "price_range", "" );
$t->set_var( "price_to_high", "" );
$t->set_var( "price_to_low", "" );

if ( isSet ( $Voucher ) )
{
    $range = $product->priceRange();
    $error = false;
    if ( ( $range->min() != 0 ) && ( $range->min() > $PriceRange ) )
    {
        $error = true;
        $t->parse( "price_to_low", "price_to_low_tpl" );
    }
    if ( ( $range->max() != 0 ) && ( $range->max() < $PriceRange ) )
    {
        $error = true;
        $t->parse( "price_to_high", "price_to_high_tpl" );
    }

    if ( !$error )
    {
        eZHTTPTool::header( "Location: /trade/voucherinformation/$ProductID/$PriceRange/$MailMethod/" );
        exit();
    }
}
else
$session->setVariable( "VoucherInformationID", 0 );

$mainImage =& $product->mainImage();
if ( $mainImage )
{
    $variation = $mainImage->requestImageVariation( $MainImageWidth, $MainImageHeight );

    $t->set_var( "main_image_id", $mainImage->id() );
    $t->set_var( "main_image_uri", "/" . $variation->imagePath() );
    $t->set_var( "main_image_width", $variation->width() );
    $t->set_var( "main_image_height", $variation->height() );
    $t->set_var( "main_image_caption", $mainImage->caption() );

    $mainImageID = $mainImage->id();

    $t->parse( "main_image", "main_image_tpl" );
}
else
{
    $t->set_var( "main_image_uri", "/sitedesign/am/img/a_100x100.gif" );
    $t->set_var( "main_image_width", "100" );
    $t->set_var( "main_image_height", "100" );
    $t->set_var( "main_image_caption", "" );

    $t->parse( "main_image", "main_image_tpl" );
}

if ( $CapitalizeHeadlines == "enabled" )
{
    include_once( "classes/eztexttool.php" );
    $t->set_var( "title_text", eZTextTool::capitalize( $product->name() ) );
}
else
{
    $t->set_var( "title_text", $product->name() );
}
$t->set_var( "intro_text", $product->brief() );
$t->set_var( "description_text", $product->description() );


// FORUM

$t->set_var( "product_forum_html", $forumHTMLContents );

if ( $product->productType() == 2 )
{
    $useVoucher = true;
    $t->set_var( "action_url", "productview" );
}
else
{
    $useVoucher = false;
    $t->set_var( "action_url", "cart/add" );
}

$images = $product->images();

$i = 0;
$t->set_var( "image", "" );
$t->set_var( "image_list", "" );
$image_count = 0;

foreach ( $images as $imageArray )
{
    $image = $imageArray["Image"];
    if ( $image->id() != $mainImageID )
    {

        if ( ( $i % 2 ) == 0 )
        {
            $t->set_var( "td_class", "bglight" );
        }
        else
        {
            $t->set_var( "td_class", "bgdark" );
        }

        $t->set_var( "image_name", $image->name() );

        $t->set_var( "image_title", $image->name() );
        $t->set_var( "image_caption", eZTextTool::nl2br( $image->caption() ) );
        $t->set_var( "image_id", $image->id() );
        $t->set_var( "product_id", $ProductID );

        $variation = $image->requestImageVariation( $SmallImageWidth, $SmallImageHeight );

        $t->set_var( "image_url", "/" .$variation->imagePath() );
        $t->set_var( "image_width", $variation->width() );
        $t->set_var( "image_height", $variation->height() );

        $t->parse( "image", "image_tpl", true );

        $image_count++;
        $i++;
    }
}

if ( $image_count > 0 )
    $t->parse( "image_list", "image_list_tpl" );

$options = $product->options();
$t->set_var( "option", "" );

$t->set_var( "value_price_header", "" );
if ( $ShowPrice and $product->showPrice() == true  )
    $t->parse( "value_price_header", "value_price_header_tpl" );

if ( $product->productType() == 3 )
{
    $t->set_var( "kjop_item", "" );
    $t->parse( "bestill_item", "bestill_item_tpl" );
}
else
{
    $t->set_var( "bestill_item", "" );
    $t->parse( "kjop_item", "kjop_item_tpl" );
}

// show alternative currencies
$currency = new eZProductCurrency( );
$currencies =& $currency->getAll();
$t->set_var( "currency_count", count( $currencies ) );
$t->set_var( "value_price_header_item", "" );
$t->set_var( "value_currency_header_item", "" );
if ( !$RequireUserLogin or get_class( $user ) == "ezuser"  )
{
    $t->parse( "value_price_header_item", "value_price_header_item_tpl" );
    if ( count( $currencies ) > 0 )
        $t->parse( "value_currency_header_item", "value_currency_header_item_tpl" );
}

$can_checkout = true;

$currency_locale = new eZLocale( $Language );

// product options removed

if ( !$product->hasQuantity( $RequireQuantity ) )
    $can_checkout = false;

// $can_checkout = $product->showPrice();


// link list
$module_link = new eZModuleLink( "eZTrade", "Product", $product->id() );
$sections =& $module_link->sections();
$t->set_var( "section_item", "" );
foreach ( $sections as $section )
{
    $t->set_var( "link_item", "" );
    $t->set_var( "section_name", $section->name() );
    $t->set_var( "section_id", $section->id() );
    $links =& $section->links();
    $i = 0;
    foreach ( $links as $link )
    {
        $t->set_var( "td_class", ($i % 2) == 0 ? "bglight" : "bgdark" );
        $t->set_var( "link_name", $link->name() );
        $t->set_var( "link_url", $link->url() );
        $t->set_var( "link_id", $link->id() );
        $t->parse( "link_item", "link_item_tpl", true );
        ++$i;
    }
    $t->parse( "section_item", "section_item_tpl", true );
}

// attribute list, optimized
$db =& eZDB::globalDatabase();

$db->array_query( $attribute_value_array, "SELECT A.ID, A.TypeID, A.Name, AV.Value, A.URL, A.Unit FROM eZTrade_Product as P, eZTrade_Attribute AS A, eZTrade_AttributeValue AS AV
WHERE
A.TypeID=P.TypeID  AND AV.AttributeID=A.ID  AND AV.ProductID=P.ID
AND P.ID='" . $product->id() .  "' ORDER BY A.Placement" );

if ( count( $attribute_value_array ) > 0 )
{
    foreach ( $attribute_value_array as $attributeValue )
    {
        if ( ( $i % 2 ) == 0 )
        {
            $t->set_var( "begin_tr", "<tr>" );
            $t->set_var( "end_tr", "" );
        }
        else
        {
            $t->set_var( "begin_tr", "" );
            $t->set_var( "end_tr", "</tr>" );
        }

        $value =& $attributeValue["Value"];
        $t->set_var( "attribute_id", $attributeValue["Name"] );
        $t->set_var( "attribute_name", $attributeValue["Name"] );
        $t->set_var( "attribute_unit", $attributeValue["Unit"] );
        $t->set_var( "attribute_value_var", $attributeValue["Value"] );

        if ( $attributeValue["URL"] == "Last ned demo" )
        {
            $t->set_var( "attribute_non_url_item", "" );
            $t->set_var( "attribute_url", $attributeValue["URL"] );
            $t->parse( "attribute_url_item", "attribute_url_item_tpl" );
        }
        else
        {
            $t->parse( "attribute_non_url_item", "attribute_non_url_item_tpl" );
            $t->set_var( "attribute_url_item", "" );
        }

        // don''t show empty attributes or attributes == 0.0
        if ( ( is_numeric( $value ) and ( $value > 0 ) ) || ( !is_numeric( $value ) and $value != "" ) )
        {
            $t->parse( "attribute", "attribute_value_tpl", true );
        }
    }
}

if ( count( $attribute_value_array ) > 0 )
{
    $t->parse( "attribute_list", "attribute_list_tpl" );
}
else
{
    $t->set_var( "attribute_list", "" );
}


$t->set_var( "product_id", $product->id() );

if ( trim( $product->externalLink() ) != "" )
{
    $t->set_var( "external_link_url", "http://" . $product->externalLink() );
    $t->parse( "external_link", "external_link_tpl" );
}
else
{
    $t->set_var( "external_link", "" );
}

$t->set_var( "product_number_item", "" );
if ( $product->productNumber() != "" )
{
    $t->set_var( "product_number", $product->productNumber() );
    $t->parse( "product_number_item", "product_number_item_tpl" );
}

$Quantity = $product->totalQuantity();
if ( is_bool( $Quantity ) and !$Quantity )
    $ShowQuantity = false;
$t->set_var( "quantity_item", "" );

if ( $ShowQuantity and $product->hasPrice() )
{
    $NamedQuantity = $Quantity;
    if ( $ShowNamedQuantity )
    {
        $NamedQuantity = eZProduct::namedQuantity( $Quantity );
    }
    $t->set_var( "product_quantity", $NamedQuantity );
    $t->parse( "quantity_item", "quantity_item_tpl" );
}

$t->set_var( "price", "" );
$t->set_var( "add_to_cart", "" );
$t->set_var( "voucher_buttons", "" );
$t->set_var( "mail_method", "" );


$t->set_var( "product_price", $product->localePrice( $PricesIncludeVAT ) );

// price code removed

// related products
{
    $productID = $product->id();
    $db =& eZDB::globalDatabase();

    $db->query( "DROP TABLE IF EXISTS eZTrade_RelatedProducts" );

    $db->query( "CREATE TEMPORARY TABLE eZTrade_RelatedProducts( OrderID int, UserID int )" );

    $db->query( "insert INTO eZTrade_RelatedProducts ( OrderID, UserID ) SELECT O.ID, O.UserID
	FROM eZTrade_OrderItem AS Item, eZTrade_Order AS O WHERE ProductID='$productID' AND Item.OrderID=O.ID" );

    $queryString = "select eZTrade_Product.Name,
eZTrade_OrderItem.ProductID,
count(*) AS PCount from eZTrade_RelatedProducts,
eZTrade_OrderItem, eZTrade_Product
WHERE eZTrade_RelatedProducts.OrderID=eZTrade_OrderItem.OrderID
AND eZTrade_OrderItem.ProductID <>'$productID'
AND eZTrade_Product.ID=eZTrade_OrderItem.ProductID
GROUP BY eZTrade_OrderItem.ProductID ORDER By PCount DESC LIMIT 10";

    $db->array_query( $related_product_array, $queryString );

    $db->query( "DROP TABLE eZTrade_RelatedProducts" );

    $t->set_var( "related_products", "" );
    foreach ( $related_product_array as $relatedProduct )
    {
        $t->set_var( "related_product_id", $relatedProduct["ProductID"] );
        $t->set_var( "related_product_name", $relatedProduct["Name"] );
        $t->parse( "related_products", "related_products_tpl", true );
        print( "her" );
    }
}

if ( ( $PurchaseProduct and !$product->discontinued() and $can_checkout ) and !$useVoucher )
    $t->parse( "add_to_cart", "add_to_cart_tpl" );
if ( ( $PurchaseProduct and !$product->discontinued() and $can_checkout ) and $useVoucher )
    $t->parse( "voucher_buttons", "voucher_buttons_tpl" );

if ( $PrintableVersion == "enabled" )
{
    $t->parse( "numbered_page_link", "numbered_page_link_tpl" );
    $t->set_var( "print_page_link", "" );
}
else
{
    $t->parse( "print_page_link", "print_page_link_tpl" );
    $t->set_var( "numbered_page_link", "" );
}

if ( isSet( $func_array ) and is_array( $func_array ) )
{
    foreach ( $func_array as $func )
    {
        $func( $t, $ProductID );
    }
}

$pathArray =& $category->path();


switch( $GlobalSectionID )
{
    case 1:
    {
        // path
        $t->set_var( "path", "" );
        foreach ( $pathArray as $path )
        {
            $t->set_var( "category_id", $path[0] );
            $t->set_var( "category_name", $path[1] );
            $t->parse( "path", "path_tpl", true );
        }
        $filename = "sitedesign/am/staticpages/";
    }
    break;
    case 2:
    {
        // path
        $t->set_var( "path", "" );
        for ( $i=0; $i < count ( $pathArray ); $i++ )
        {
            if ( $i == 0 )
            {
                $t->set_var( "path_url", "/musikk/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            elseif ( $i == 1 )
            {
                $t->set_var( "path_url", "/musikk/productlist/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            elseif ( $i == 2 )
            {
                $t->set_var( "path_url", "/musikk/productlist/" . $pathArray[$i-1][0] . "/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            else
            {
                $t->set_var( "path_url", "/trade/productlist/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
        }

        $filename = "sitedesign/am/staticpages/musikk_content.html";
    }
    break;
    case 3:
    {
        // path
        $t->set_var( "path", "" );
        for ( $i=0; $i < count ( $pathArray ); $i++ )
        {
            if ( $i == 0 )
            {
                $t->set_var( "path_url", "/dvd/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            else
            {
                $t->set_var( "path_url", "/trade/productlist/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
        }

        $filename = "sitedesign/am/staticpages/dvd_content.html";
    }
    break;
    case 4:
    {
        // path
        $t->set_var( "path", "" );
        for ( $i=0; $i < count ( $pathArray ); $i++ )
        {
            if ( $i == 0 )
            {
                $t->set_var( "path_url", "/hi-fi/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            else
            {
                $t->set_var( "path_url", "/trade/productlist/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
        }

        $filename = "sitedesign/am/staticpages/hifi_content.html";
    }
    break;
    case 5:
    {
        // path
        $t->set_var( "path", "" );
        foreach ( $pathArray as $path )
        {
            $t->set_var( "category_id", $path[0] );
            $t->set_var( "category_name", $path[1] );
            $t->parse( "path", "path_tpl", true );

        }

        $filename = "sitedesign/am/staticpages/multimedia_content.html";
    }
    break;

    case 6:
    {
        // path
        $t->set_var( "path", "" );
        for ( $i=1; $i < count ( $pathArray ); $i++ )
        {
            if ( $i == 1 )
            {
                $t->set_var( "path_url", "/multimedia/playstation/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            elseif ( $i == 2 )
            {
            }
            else
            {
                $t->set_var( "path_url", "/trade/productlist/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
        }

        $filename = "sitedesign/am/staticpages/playstation_content.html";
    }
    break;


    case 7:
    {
        // path
        $t->set_var( "path", "" );
        for ( $i=1; $i < count ( $pathArray ); $i++ )
        {
            if ( $i == 1 )
            {
                $t->set_var( "path_url", "/multimedia/pc/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            elseif ( $i == 2 )
            {
            }
            else
            {
                $t->set_var( "path_url", "/trade/productlist/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
        }

        $filename = "sitedesign/am/staticpages/pc_content.html";
    }
    break;


    case 8:
    {
        // path
        $t->set_var( "path", "" );
        for ( $i=1; $i < count ( $pathArray ); $i++ )
        {
            if ( $i == 1 )
            {
                $t->set_var( "path_url", "/multimedia/nintendo/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            elseif ( $i == 2 )
            {
            }
            else
            {
                $t->set_var( "path_url", "/trade/productlist/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
        }

        $filename = "sitedesign/am/staticpages/nintendo_content.html";
    }
    break;


    case 9:
    {
        // path
        $t->set_var( "path", "" );
        for ( $i=1; $i < count ( $pathArray ); $i++ )
        {
            if ( $i == 1 )
            {
                $t->set_var( "path_url", "/multimedia/xbox/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            elseif ( $i == 2 )
            {
            }
            else
            {
                $t->set_var( "path_url", "/trade/productlist/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
        }

        $filename = "sitedesign/am/staticpages/xbox_content.html";
    }
    break;

    case 11:
    {
        // path
        $t->set_var( "path", "" );
        for ( $i=1; $i < count ( $pathArray ); $i++ )
        {
            if ( $i == 1 )
            {
                $t->set_var( "path_url", "/multimedia/sega/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
            elseif ( $i == 2 )
            {
            }
            else
            {
                $t->set_var( "path_url", "/trade/productlist/" . $pathArray[$i][0] . "/" );
                $t->set_var( "category_name", $pathArray[$i][1] );
                $t->parse( "path", "path_tpl", true );
            }
        }
        $filename = "sitedesign/am/staticpages/sega_content.html";
    }
    break;

    default:
    {
        // path
        $t->set_var( "path", "" );
        foreach ( $pathArray as $path )
        {
            $t->set_var( "category_id", $path[0] );
            $t->set_var( "category_name", $path[1] );
            $t->parse( "path", "path_tpl", true );

            $SiteTitleAppend .= $path[1] . " - ";
        }
    }
}

if ( file_exists ( $filename ) )
{
    $file = eZFile::fopen( $filename, "r" );
    if ( $file )
    {
        $content =& fread( $file, eZFile::filesize( $filename ) );
        fclose( $file );
    }
}
$t->set_var( "content", "$content" );

$SiteTitleAppend = $product->name();
$SiteDescriptionOverride = str_replace( "\"", "", strip_tags( $product->brief() ) );
$SiteKeywordsOverride = str_replace( "\"", "", strip_tags( $product->keywords() ) );

if ( $GenerateStaticPage == "true" && !$useVoucher )
{
    $template_var = "product_view_tpl";

    // add PHP code in the cache file to store variables
    $output = "<?php\n";
    $output .= "\$GlobalSectionID=\"$GlobalSectionID\";\n";
    $output .= "\$SiteTitleAppend=\"$SiteTitleAppend\";\n";
    $output .= "\$SiteDescriptionOverride=\"$SiteDescriptionOverride\";\n";
    $output .= "\$SiteKeywordsOverride=\"$SiteKeywordsOverride\";\n";
    $output .= "\$SimilarCategoryID=\"$SimilarCategoryID\";\n";
    $output .= "?>\n";
    $output =& $t->parse($target, $template_var );
    // print the output the first time while printing the cache file.
    print( $output );
    $CacheFile->store( &$output );
}
else
{
    $t->pparse( "output", "product_view_tpl" );
}

?>
