<table width="747" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img src="/sitedesign/am/img/h_musikk.gif" width="370" height="26"></td>
		<td width="7"><img src="/sitedesign/am/img/spacer.gif" width="7" height="26"></td>
		<td><img src="/sitedesign/am/img/h_multimedia.gif" width="370" height="26"></td>
	</tr>
	<tr>
		<td bgcolor="#FEF4E9" valign="top" class="frontpagebox">
			<table width="345" border="0" cellspacing="0" cellpadding="0">
				<tr valign="top">
					<td width="110"><img src="/sitedesign/am/img/bilder/madonna.jpg" width="100" height="100" class="img"></td>
					<td class="normal"><a href="#" class="smalltitle">MADONNA: GHV2</a><br>
						Denne samlingen tar for seg perioden etter &quot;Erotica&quot; og &quot;The Immaculate Collection&quot;.<br>
						<span class="price"><img src="/sitedesign/am/img/b_kjop.gif" width="38" height="12" align="right">kr.149,-</span> </td>
				</tr>
				<tr>
					<td colspan="2"><img src="/sitedesign/am/img/spacer.gif" width="50" height="10"></td>
				</tr>
				<tr>
					<td colspan="2"><img src="/sitedesign/am/img/musikk_nyheter.gif" width="55" height="8" vspace="1"></td>
				</tr>
				<tr>
					<td colspan="2" class="smallmusicbox">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td rowspan="6" valign="top" width="50"><img src="/sitedesign/am/img/bilder/britney1.jpg" width="46" height="45" class="img" vspace="5"></td>
								<td class="normal">Morten Abel</td>
								<td class="normal"><a href="#">I'll Come...</a></td>
								<td class="normal">Kr 149,-</td>
								<td><span class="price"><img src="/sitedesign/am/img/b_kjop.gif" width="38" height="12"></span></td>
							</tr>
							<tr>
								<td class="normal">Cher</td>
								<td class="normal"><a href="#">Living Proof</a></td>
								<td class="normal">Kr 149,-</td>
								<td><span class="price"><img src="/sitedesign/am/img/b_kjop.gif" width="38" height="12"></span></td>
							</tr>
							<tr>
								<td class="normal">Mick Jagger</td>
								<td class="normal"><a href="#">Goddess</a></td>
								<td class="normal">Kr 149,-</td>
								<td><span class="price"><img src="/sitedesign/am/img/b_kjop.gif" width="38" height="12"></span></td>
							</tr>
							<tr>
								<td class="normal">Stina Nordenstam</td>
								<td class="normal"><a href="#">This is</a></td>
								<td class="normal">Kr 149,-</td>
								<td><span class="price"><img src="/sitedesign/am/img/b_kjop.gif" width="38" height="12"></span></td>
							</tr>
							<tr>
								<td class="normal">The Cure</td>
								<td class="normal"><a href="#">Greatest Hits</a></td>
								<td class="normal">Kr 149,-</td>
								<td><span class="price"><img src="/sitedesign/am/img/b_kjop.gif" width="38" height="12"></span></td>
							</tr>
							<tr>
								<td class="normal">Kelis</td>
								<td class="normal"><a href="#">Wanderland</a></td>
								<td class="normal">Kr 149,-</td>
								<td><span class="price"><img src="/sitedesign/am/img/b_kjop.gif" width="38" height="12"></span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="7"><img src="/sitedesign/am/img/spacer.gif" width="7" height="260"></td>
		<td bgcolor="#FEF4E9" valign="top" class="frontpagebox">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3"><img src="/sitedesign/am/img/spacer.gif" width="370" height="7"></td>
	</tr>
	<tr>
		<td><img src="/sitedesign/am/img/h_film.gif" width="370" height="26"></td>
		<td><img src="/sitedesign/am/img/spacer.gif" width="7" height="26"></td>
		<td><img src="/sitedesign/am/img/h_hifi.gif" width="370" height="26"></td>
	</tr>
	<tr>
		<td bgcolor="#FEF4E9" valign="top" class="frontpagebox">&nbsp;</td>
		<td><img src="/sitedesign/am/img/spacer.gif" width="7" height="260"></td>
		<td bgcolor="#FEF4E9" valign="top" class="frontpagebox">&nbsp;</td>
	</tr>
</table>

<!-- BEGIN header_item_tpl -->

<!-- BEGIN latest_headline_tpl -->	

<!-- END latest_headline_tpl -->	
<!-- BEGIN category_headline_tpl -->	

<!-- END category_headline_tpl -->

<!-- END header_item_tpl -->

<!-- BEGIN path_item_tpl -->

<!-- END path_item_tpl -->

<!-- BEGIN current_image_item_tpl -->

<!-- END current_image_item_tpl -->

<!-- BEGIN category_list_tpl -->

<!-- BEGIN category_item_tpl -->

<!-- BEGIN image_item_tpl -->

<!-- END image_item_tpl -->

<!-- BEGIN no_image_tpl -->

<!-- END no_image_tpl -->

<!-- END category_item_tpl -->

<!-- END category_list_tpl -->

<!-- BEGIN article_list_tpl -->

<!-- BEGIN article_item_tpl -->

<!-- BEGIN headline_with_link_tpl -->

<!-- END headline_with_link_tpl -->
<!-- BEGIN headline_without_link_tpl -->

<!-- END headline_without_link_tpl -->
<!-- BEGIN article_date_tpl -->

<!-- END article_date_tpl -->

<!-- BEGIN article_image_tpl -->

<!-- END article_image_tpl -->

<!-- BEGIN read_more_tpl -->

<!-- END read_more_tpl -->

<!-- BEGIN article_topic_tpl -->

<!-- END article_topic_tpl -->

<!-- END article_item_tpl -->

<!-- END article_list_tpl -->



<!-- BEGIN type_list_tpl -->

<!-- BEGIN type_list_previous_tpl -->

<!-- END type_list_previous_tpl -->

<!-- BEGIN type_list_previous_inactive_tpl -->

<!-- END type_list_previous_inactive_tpl -->

<!-- BEGIN type_list_item_list_tpl -->

<!-- BEGIN type_list_item_tpl -->

<!-- END type_list_item_tpl -->

<!-- BEGIN type_list_inactive_item_tpl -->

<!-- END type_list_inactive_item_tpl -->

<!-- END type_list_item_list_tpl -->

<!-- BEGIN type_list_next_tpl -->

<!-- END type_list_next_tpl -->

<!-- BEGIN type_list_next_inactive_tpl -->

<!-- END type_list_next_inactive_tpl -->

<!-- END type_list_tpl -->
