<h1>{product_name}</h1>
<hr noshade="noshade" size="1" />

<!-- BEGIN errors_tpl -->
<p class="error">{intl-please_fix}:</p><br />
<ol>
<!-- BEGIN price_to_high_tpl -->
<li>{intl-price_to_high}
<!-- END price_to_high_tpl -->
<!-- BEGIN price_to_low_tpl -->
<li>{intl-price_to_low}
<!-- END price_to_low_tpl -->
<!-- BEGIN missing_fields_tpl -->
<li>{intl-missing_fields}
<!-- END missing_fields_tpl -->
<!-- BEGIN too_many_letters_tpl -->
<li>{intl-too_many_letters1} {text_size} {intl-too_many_letters2} {text_limit} {intl-too_many_letters3}
<!-- END too_many_letters_tpl -->
</ol>
<!-- END errors_tpl -->


<p>{intl-description}</p>
<br />

<form action="{www_dir}{index}/trade/voucherinformation/{product_id}/{mail_method}/{voucher_info_id}" method="post">


<table>
<tr>
    <td>
    <b>{intl-price_range}:</b><br />
    <input type="text" name="PriceRange" value="{price_range}" />
    </td>
</tr>
</table>
<hr noshade="noshade" size="1" />

<!-- BEGIN email_tpl -->
<table width="1%">
  <tr>
    <td>
      <b>{intl-name}:</b><br />
      <input type="text" name="ToName" value="{to_name}" />
    </td>
    <td>
      <b>{intl-email}:</b><br />
      <input type="text" name="Email" value="{to_email}" />
    </td>
  </tr>
  <tr>
    <td>
      <b>{intl-from_name}:</b><br />
      <input type="text" name="FromName" value="{from_name}" />
    </td>
    <td>
      <b>{intl-from_email}:</b><br />
      <input type="text" name="FromEmail" value="{from_email}" />    
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <b>{intl-text}:</b><br />
      <textarea cols="60" name="Description" rows="8">{description}</textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2" class="small">{intl-attention}</td>
  </tr>
</table>
<!-- END email_tpl -->


<!-- BEGIN smail_tpl -->
<table width="60%" cellspacing="0" cellpadding="4" border="0">
<tr>
  <td width="50%">
    <table width="100%" cellpadding="1" cellspacing="0" bgcolor="#003366">
    <tr>
    <td>  
    <table width="100%" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF">
    <tr>
    <th style="padding-top: 1px; padding-bottom: 1px">{intl-to_header}</th>
    </tr>
    <tr>
    <td width="1%">
     <b class="medium">{intl-name}:</b><br />
     <input class="medium" style="width: 100%" type="text" name="ToName" value="{to_name_value}" />
     <br />
     <b class="medium">{intl-street}:</b><br />
     <input class="medium" style="width: 100%" type="text" name="ToStreet1" value="{to_street1_value}"/>
     <br />
     <table cellpadding="0" cellspacing="0" border="0" width="100%">
     <tr>
     <td width="30%">
     <b class="medium">{intl-zip}:</b><br />     
     <input class="medium" type="text" style="width: 100%" name="ToZip" value="{to_zip_value}"/>
     </td>
     <td>&nbsp;</td>
     <td width="70%">
     <b class="medium">{intl-place}:</b><br />          
     <input class="medium" type="text" style="width: 100%" name="ToPlace" value="{to_place_value}"/>
     </td>
     </tr>
     </table>
     <!-- BEGIN to_country_tpl -->
     <b class="medium">{intl-country}:</b><br />
     <select class="medium" style="width: 100%" name="ToCountryID">
     <!-- BEGIN to_country_option_tpl -->
     <option {to_is_selected} value="{country_id}">{country_name}</option>
     <!-- END to_country_option_tpl -->
     </select>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    
     <!-- END to_country_tpl -->
     </td>
  <td width="50%">
    <table width="100%" cellpadding="1" cellspacing="0" bgcolor="#003366">
    <tr>
    <td>  
    <table width="100%" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF">
    <tr>
    <th style="padding-top: 1px; padding-bottom: 1px">{intl-from_header}</th>
    </tr>
    <tr>
    <td>     
     <b class="medium">{intl-name}:</b><br />
     <input class="medium" style="width: 100%" type="text" size="20" name="FromName" value="{from_name_value}" />
     <br />
     <b class="medium" >{intl-street}:</b><br />
     <input class="medium" style="width: 100%" type="text" size="20" name="FromStreet1" value="{from_street1_value}"/>
     <br />     
     <table cellpadding="0" cellspacing="0" border="0" width="100%">
     <tr>
     <td width="30%">
     <b class="medium">{intl-zip}:</b><br />     
     <input class="medium" type="text" style="width: 100%" name="FromZip" value="{from_zip_value}"/>
     </td>
     <td>&nbsp;</td>
     <td width="70%">
     <b class="medium">{intl-place}:</b><br />          
     <input class="medium" type="text" style="width: 100%" name="FromPlace" value="{from_place_value}"/>
     </td>
     </tr>
     </table>
     <!-- BEGIN from_country_tpl -->
     <b class="medium">{intl-country}:</b><br />
     <select class="medium" style="width: 100%"  name="FromCountryID">
     <!-- BEGIN from_country_option_tpl -->
     <option {from_is_selected} value="{country_id}">{country_name}</option>
     <!-- END from_country_option_tpl -->
     </select>
      </td>
     </tr>
     </table>
      </td>
     </tr>
     </table>


     <!-- END from_country_tpl -->
     </td>
</tr>
</table>
<hr noshade="noshade" size="1" />
<b>{intl-text}:</b><br />
<textarea name="Description" cols="40" rows="8">{description}</textarea>
<br />
<span class="small">{intl-text_limit1} {text_limit} {intl-text_limit2}</span>
<br /><br />
<!-- END smail_tpl -->

<hr noshade="noshade" size="1" />

<input type="hidden" name="Mail" value="{mail_method}" />
<input type="hidden" name="ProductID" value="{product_id}" />

<input class="okbutton" type="submit" name="OK" value="{intl-ok}" />&nbsp;
<input class="okbutton" type="submit" name="Cancel" value="{intl-cancel}" />


</form>