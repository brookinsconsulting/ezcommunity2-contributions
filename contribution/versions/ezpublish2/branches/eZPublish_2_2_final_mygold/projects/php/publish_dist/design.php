<?

switch( $design )
{
    case "GP":
    {
        $design = "Gravurplatte";
    }
    break;
    case "HE":
    {
        $design = "Herz";
    }
    break;
    case "HU":
    {
        $design = "Hund";
    }
    break;

    case "KZ":
    {
        $design = "Kreuz";
    }
    break;

    case "MD":
    {
        $design = "Medaillon";
    }
    break;

    case "SP":
    {
        $design = "Sport";
    }
    break;

    case "SZ":
    {
        $design = "Sternzeichen";
    }
    break;

    case "TI":
    {
        $design = "Tier";
    }
    break;

    case "1C0":
    {
        $design = "Solit�r > 1,00 Carat";
    }
    break;

    case "1C5":
    {
        $design = "Solit�r < 1,9 Carat";
    }
    break;

    case "BR00":
    {
        $design = "DiaBes < 0,20 Carat";
    }
    break;

    case "BR1":
    {
        $design = "DiaBes < 1,90 Carat";
    }
    break;

    case "BR2":
    {
        $design = "DiaBes < 2,90 Carat";
    }
    break;

    case "BR25":
    {
        $design = "DiaBes < 0,45 Carat";
    }
    break;

    case "BR50":
    {
        $design = "DiaBes < 0,95 Carat";
    }
    break;

    case "C00":
    {
        $design = "Solit�r < 0,05 Carat";
    }
    break;

    case "C05":
    {
        $design = "Solit�r < 0,10 Carat";
    }
    break;

    case "C15":
    {
        $design = "Solit�r < 0,24 Carat";
    }
    break;

    case "C25":
    {
        $design = "Solit�r < 0,39 Carat";
    }
    break;

    case "C50":
    {
        $design = "Solit�r < 0,59 Carat";
    }
    break;

    case "C60":
    {
        $design = "Solit�r < 0,74 Carat";
    }
    case "C75":
    {
        $design = "Solit�r < 0,99 Carat";
    }
    break;

    case "AY":
    {
        $design = "sonstige AkoyaZuchtperle";
    }
    break;

    case "AY0":
    {
        $design = "AkoyaZuchtperle > 10,0mm";
    }
    break;
                    
    case "AY2":
    {
        $design = "AkoyaZuchtperle < 2,5mm";
    }
    break;

    case "AY3":
    {
        $design = "AkoyaZuchtperle 3,0-3,5mm";
    }
    break;

    case "AY4":
    {
        $design = "AkoyaZuchtperle 4,0-4,5mm";
    }
    break;

    case "AY5":
    {
        $design = "AkoyaZuchtperle 5,0-5,5mm";
    }
    break;

    case "AY6":
    {
        $design = "AkoyaZuchtperle 6,0-6,5mm";
    }
    break;

    case "AY7":
    {
        $design = "AkoyaZuchtperle 7,0-7,5mm";
    }
    break;

    case "AY8":
    {
        $design = "AkoyaZuchtperle 8,0-8,5mm";
    }
    break;

    case "AY9":
    {
        $design = "AkoyaZuchtperle 9,0-9,5mm";
    }
    break;

    case "BW0":
    {
        $design = "S�sswasserPerle > 10,0mm";
    }
    break;

    case "BW3":
    {
        $design = "S�sswasserPerle < 4,0mm";
    }
    break;

    case "BW4":
    {
        $design = "S�sswasserPerle 4,0-4,5mm";
    }
    break;

    case "BW5":
    {
        $design = "S�sswasserPerle 5,0-5,5mm";
    }
    break;

    case "BW6":
    {
        $design = "S�sswasserPerle 6,0-6,5mm";
    }
    break;

    case "BW7":
    {
        $design = "S�sswasserPerle 7,0-7,5mm";
    }
    break;

    case "BW8":
    {
        $design = "S�sswasserPerle 8,0-8,5mm";
    }
    break;

    case "BW9":
    {
        $design = "S�sswasserPerle 9,0-9,5mm";
    }
    break;

    case "TA":
    {
        $design = "TahitiZuchtperle";
    }
    break;

    case "BR00":
    {
        $design = "DiaBes < 0,20 Carat";
    }
    break;

    case "C00":
    {
        $design = "Solit�r < 0,05 Carat";
    }
    break;

    case "C05":
    {
        $design = "Solit�r < 0,10 Carat";
    }
    break;

    case "C15":
    {
        $design = "Solit�r < 0,24 Carat";
    }
    break;

    case "KH":
    {
        $design = "Krawattennadel/schieber";
    }
    break;

    case "KN":
    {
        $design = "Kragenecken";
    }
    break;

    case "MK":
    {
        $design = "Manschettenkn�pfe";
    }
    break;

    case "ZI":
    {
        $design = "Zirkonia";
    }
    break;

    case "C00":
    {
        $design = "< 0,05 Carat";
    }
    break;

    case "C05":
    {
        $design = "< 0,10 Carat";
    }
    break;

    case "C10":
    {
        $design = "< 0,15 Carat";
    }
    break;

    case "C15":
    {
        $design = "< 0,24 Carat";
    }
    break;

    case "C25":
    {
        $design = "< 0,48 Carat";
    }
    break;

    case "AK":
    {
        $design = "Anker";
    }
    break;

    case "DP":
    {
        $design = "Deutschpanzer";
    }
    break;

    case "EB":
    {
        $design = "Erbs";
    }
    break;

    case "FA":
    {
        $design = "Fantasie";
    }
    break;

    case "FI":
    {
        $design = "Figaro";
    }
    break;

    case "FP":
    {
        $design = "Flachpanzer";
    }
    break;

    case "FU":
    {
        $design = "Fuchsschwanz";
    }
    break;

    case "GA":
    {
        $design = "Garibaldi";
    }
    break;

    case "HA":
    {
        $design = "Haferkorn";
    }
    break;

    case "HI":
    {
        $design = "Himbeer";
    }
    break;

    case "KD":
    {
        $design = "Kordelkette";
    }
    break;

    case "KK":
    {
        $design = "K�nigskette";
    }
    break;

    case "MA":
    {
        $design = "Marina";
    }
    break;

    case "PU":
    {
        $design = "Puzzlekette";
    }
    break;

    case "SL":
    {
        $design = "Schlangen";
    }
    break;

    case "SP":
    {
        $design = "Stegpanzer";
    }
    break;

    case "SR":
    {
        $design = "Strumpf-/Strickliesel";
    }
    break;

    case "TO":
    {
        $design = "Tonda";
    }
    break;

    case "VZ":
    {
        $design = "Venezia";
    }
    break;

    case "AM":
    {
        $design = "Amethyst";
    }
    break;

    case "AMZI":
    {
        $design = "Amethyst/Zirkonia";
    }
    break;

    case "AQ":
    {
        $design = "Aquamarin";
    }
    break;

    case "AY":
    {
        $design = "Akoya-Zp";
    }
    break;
    case "AYBR":
    {
        $design = "Akoya/Diamant";
    }
    break;

    case "AYZI":
    {
        $design = "Zirkonia/Akoya";
    }
    break;

    case "BT":
    {
        $design = "Blautopas";
    }
    break;

    case "BTAY":
    {
        $design = "Blautopas/Akoya";
    }
    break;

    case "BTBR":
    {
        $design = "Blautopas/Diamant";
    }
    break;

    case "BTBW":
    {
        $design = "Blautopas/S��wasser";
    }
    break;

    case "BTZI":
    {
        $design = "Blautopas/Zirkonia";
    }
    break;

    case "BW":
    {
        $design = "S�sswasser";
    }
    break;

    case "BWBR":
    {
        $design = "S�sswasserZP/Diamant";
    }
    break;

    case "BWZI":
    {
        $design = "Zirkonia/S��wasserZP";
    }
    break;

    case "CI":
    {
        $design = "Citrin";
    }
    break;

    case "CIAY":
    {
        $design = "Citrin/Akoya";
    }
    break;

    case "GT":
    {
        $design = "Granat";
    }
    break;

    case "GTBW":
    {
        $design = "Granat/S��wasser";
    }
    break;

    case "MB":
    {
        $design = "MabePerle";
    }
    break;

    case "MBBR":
    {
        $design = "MabePerle/Diamant";
    }
    break;

    case "PE":
    {
        $design = "Peridot";
    }
    break;

    case "PM":
    {
        $design = "Perlmutt";
    }
    break;

    case "RSBR":
    {
        $design = "Rubin/Safir/Diamant";
    }
    break;

    case "RU":
    {
        $design = "Rubin";
    }
    break;

    case "RUBR":
    {
        $design = "Rubin/Diamant";
    }
    break;

    case "SA":
    {
        $design = "Saphir";
    }
    break;

    case "SABR":
    {
        $design = "Saphir/Diamant";
    }
    break;

    case "SM":
    {
        $design = "Smaragd";
    }
    break;

    case "SMBR":
    {
        $design = "Smaragd/Diamant";
    }
    break;

    case "SP":
    {
        $design = "Spinell";
    }
    break;

    case "SPZI":
    {
        $design = "Spinell/Zirkonia";
    }
    break;

    case "SSBR":
    {
        $design = "Safir/Smaragd/Diamant";
    }
    break;

    case "TA":
    {
        $design = "TahitiPerle";
    }
    break;

    case "ZI":
    {
        $design = "Zirkonia";
    }
    break;
    default:
    {
        if ( $design )
            eZLog::writeWarning( "Product: " . $productNumber . ". Could not found a design case for: " . $design );
    }
    break;
}
?>
