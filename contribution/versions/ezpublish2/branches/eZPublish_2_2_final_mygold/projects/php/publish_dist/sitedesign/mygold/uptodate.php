<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr> 
    <th>Liefergarantie</th>
  </tr>
  <tr> 
    <td class="spacer2">&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr> 
          <td valign="top" class="small">
	    <table border="0" width="75" cellspacing="0" cellpadding="0" align="left">
	      <tr>
	        <td width="1">
		  <table border="0" cellspacing="0" cellpadding="1" bgcolor="#003366">
	            <tr>
	              <td><img src="/sitedesign/mygold/images/ostern.jpg" align="left" width="68" height="56" alt="Ostern bei MyGold.com" /></td>
	            </tr>
	          </table>
		</td>
	      </tr>
	    </table>
            <b>Am 31. M&auml;rz ist Ostern!<br />
	    Schenken Sie doch etwas Besonderes, denn Schmuck ist unverg&auml;nglich!<br />
	    Alle Bestellungen die bis Mittwoch, den 27.3. 12:00 Uhr bei uns eingehen, werden Ihnen
	    rechtzeitig zu Ostern geliefert. Lesen Sie bitte unsere <a href="/article/articleview/49/">Liefergarantie</a>
	    f&uuml;r weitere Informationen.<br />
	    F&uuml;r die Unentschlossenen haben wir ein <a href="/trade/productspecials/1/">Osterangebot</a> zusammengestellt.	    
            </b>
	  </td>
	</tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="spacer2">&nbsp;</td>
  </tr>
  <tr> 
    <td class="bgspacer" ><img src="/sitedesign/mygold/images/shim.gif" width="1" height="2" alt="" /></td>
  </tr>
  <tr> 
    <td class="spacer5">&nbsp;</td>
  </tr>
</table>
																								      