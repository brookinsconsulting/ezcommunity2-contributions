<!-- BEGIN address_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
<a class="path" href="{www_dir}{index}/trade/checkout/address/">{intl-address-path}</a>
<!-- END address_path_tpl -->

<!-- BEGIN address_dummy_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
{intl-address-path}
<!-- END address_dummy_path_tpl -->

<!-- BEGIN shipping_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
<a class="path" href="{www_dir}{index}/trade/checkout/shipping/">{intl-shipping-path}</a>
<!-- END shipping_path_tpl -->

<!-- BEGIN shipping_dummy_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
{intl-shipping-path}
<!-- END shipping_dummy_path_tpl -->

<!-- BEGIN packing_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
<a class="path" href="{www_dir}{index}/trade/checkout/packing/">{intl-packing-path}</a>
<!-- END packing_path_tpl -->

<!-- BEGIN packing_dummy_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
{intl-packing-path}
<!-- END packing_dummy_path_tpl -->

<!-- BEGIN payment_method_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
<a class="path" href="{www_dir}{index}/trade/checkout/paymentmethod/">{intl-payment_method-path}</a>
<!-- END payment_method_path_tpl -->

<!-- BEGIN payment_method_dummy_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
{intl-payment_method-path}
<!-- END payment_method_dummy_path_tpl -->

<!-- BEGIN overview_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
<a class="path" href="{www_dir}{index}/trade/checkout/overview/">{intl-overview-path}</a>
<!-- END overview_path_tpl -->

<!-- BEGIN overview_dummy_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
{intl-overview-path}
<!-- END overview_dummy_path_tpl -->

<!-- BEGIN payment_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
<a class="path" href="{www_dir}{index}/trade/checkout/payment/">{intl-payment-path}</a>
<!-- END payment_path_tpl -->

<!-- BEGIN payment_dummy_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
{intl-payment-path}
<!-- END payment_dummy_path_tpl -->

<!-- BEGIN ordersent_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
<a class="path" href="{www_dir}{index}/trade/checkout/ordersent/">{intl-ordersent-path}</a>
<!-- END ordersent_path_tpl -->

<!-- BEGIN ordersent_dummy_path_tpl -->
<img src="{www_dir}/images/path-slash.gif" height="10" width="16" border="0" alt="0" />
{intl-ordersent-path}
<!-- END ordersent_dummy_path_tpl -->

<br /><br />
<hr noshade="noshade" size="4" />

<form action="{www_dir}{index}/trade/checkout/packing/" method="post">
<table class="list" width="100%" cellspacing="0" cellpadding="4" border="0">

<!-- BEGIN packing_list_tpl -->
<tr>
<!-- BEGIN packing_item_tpl -->
	<td class="{td_class}">
	<!-- BEGIN image_tpl -->
    	  <img src="{www_dir}{thumbnail_image_uri}" border="0" width="{thumbnail_image_width}" height="{thumbnail_image_height}" alt="{thumbnail_image_caption}"/>
	<!-- END image_tpl -->
	<!-- BEGIN no_image_tpl -->

	<!-- END no_image_tpl -->
	<br />
	<b>Name:</b> {packing_name}<br />

	<b>Description </b> {packing_description}<br />

	<b>Price </b> {packing_price}<br />
	</td>
	<td class="{td_class}">
	<input type="radio" name="PackingTypeID" value="{id}"/>
	</td>
<!-- END packing_item_tpl -->
</tr>
<!-- END packing_list_tpl -->
</table>

<br /><br />
<hr noshade="noshade" size="4" />
<input type="submit" name="Next" value="{intl-next}" />

</form>