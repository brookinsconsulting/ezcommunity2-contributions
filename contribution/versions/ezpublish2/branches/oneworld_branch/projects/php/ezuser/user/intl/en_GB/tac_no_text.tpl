<p>Since you didn't accept our terms of service we can not register you as a new user of our service.</p>
<p>If you should change your mind you could try <a href="/user/userwithaddress/new/">registering again</a>.</p>
<p>You can still enjoy much of our site without registering, please go to our <a href="/">main page</a> to
continue enjoying the parts of our site which doesn't need a registration.</p>

