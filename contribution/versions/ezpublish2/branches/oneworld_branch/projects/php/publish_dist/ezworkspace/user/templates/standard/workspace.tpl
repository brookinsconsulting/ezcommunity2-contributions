<h2>{intl-user_info}</h2>

{name}
<br>
<a href="mailto:{email}">{email}</a>

<br>


<h2>{intl-latest_articles}</h2>
<!-- BEGIN article_tpl -->
<a href="/article/articleview/{article_id}">{article_name}</a><br>
<!-- END article_tpl -->

<h2>{intl-latest_forum_messages}</h2>
<!-- BEGIN forum_tpl -->
<a href="/forum/message/{message_id}">{message_name}</a><br>
<!-- END forum_tpl -->
